<?
include 'inicio.php';
?>

<div class="fakebanner"></div>

<?
$banners = Banners::get("id_category = 2 AND status = 1");
?>

<div class="slider_home">
    <ul id="slider_home<?= (count($banners) == 1) ? "_not" : "" ; ?>">
        <?
        foreach ($banners as $banner){
            $banner_id = $banner["id"];
            $image = Banners::get_image($banner_id, "description ASC");
            $image_path = Banners::image_path($image[0]["image"]);
            ?>
            <li style="background-image: url('<?= $image_path ?>')"></li>
            <?
        }
        ?>
    </ul>

    <div class="">
        <span class="scroll-btn">
            <span class="mouse">
                <span></span>
            </span>
        </span>
    </div>
</div>


<!-- About Us -->
<div class="bloco wow fadeInUp" data-wow-delay="1s">
    <div class="container">

        <?
        $pagina = Pages::get_by_id(3);
        $image = Pages::get_image($pagina["id"]);
        $image_path = Pages::image_path($image[0]["image"]);
        ?>

        <div class="col-md-12 center">
            <div class="txt_titulo"><?= $pagina["name"]?></div>
        </div>

        <br>

        <div class="col-md-4 center img_foto">
            <a href="<?= $image_path?>" class="fancybox" data-fancybox="CEO" title="CEO - Nuno Ribeirinho">
                <img src="<?= $image_path?>" />
            </a>
            <div class="txt_texto">CEO - Nuno Ribeirinho</div>
        </div>

        <div class="col-md-8">
            <div class="txt_texto"><?= $pagina["description"]?></div>

            <br><br>

            <div class="txt_texto"><?= $pagina["content"]?></div>
        </div>

    </div>
</div>

<!-- Worldwide Shipping -->
<div class="bloco wow fadeInUp blue_back" data-wow-delay="1s">
    <div class="container">

        <?
        $pagina = Pages::get_by_id(2);
        $image = Pages::get_image($pagina["id"]);
        $image_path = Pages::image_path($image[0]["image"]);
        ?>

        <div class="col-md-12 center">
            <div class="txt_titulo white"><?= $pagina["name"]?></div>
        </div>

        <br>

        <div class="col-md-12">
            <div class="txt_texto white"><?= $pagina["content"]?></div>
        </div>

        <br><br>

        <div class="col-md-12 center atlas">
            <img src="<?= $image_path?>" />
        </div>

    </div>
</div>



<?
include 'fim.php';
?>



