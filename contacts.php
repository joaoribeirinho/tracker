<?
include 'inicio.php';
?>

<div class="fakebanner"></div>

<?
$banners = Banners::get("id_category = 5 AND status = 1");
?>

<div class="slider_home">
    <ul id="slider_home<?= (count($banners) == 1) ? "_not" : "" ; ?>">
        <?
        foreach ($banners as $banner){
            $banner_id = $banner["id"];
            $image = Banners::get_image($banner_id, "description ASC");
            $image_path = Banners::image_path($image[0]["image"]);
            ?>
            <li style="background-image: url('<?= $image_path ?>')"></li>
            <?
        }
        ?>
    </ul>

    <div class="">
        <span class="scroll-btn">
            <span class="mouse">
                <span></span>
            </span>
        </span>
    </div>
</div>

<div class="bloco wow fadeInUp" data-wow-delay="1s">
    <div class="container">

        <div class="row pad_only_bot">
            <div class="col-md-12 txt_titulo center">Contacts</div>
        </div>

        <div class="row">
            <div class="col-md-12 contactos">

                <div class="txt_texto">How you can reach us:</div>

                <br>

                <div class="txt_texto">
                    <i class="fa fa-phone" aria-hidden="true"></i> +351 7835 689507
                </div>

                <br>

                <div class="txt_texto">
                    <i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:reefpor@gmail.com">reefpor@gmail.com</a>
                </div>

                <div id="target"></div>

                <br><br>

                <div class="txt_texto">You can also find me on these social networks. I'm not afraid to show you who I
                    am and that's the whole purpose of this, so you can see what I'm involved with in my daily basis.</div>

                <?/*<br>

                <div class="redes">
                    <a href="https://www.instagram.com/joaoribeirinho/" target="_blank">
                        <i class="fa fa-instagram green" aria-hidden="true" style="font-size:30px;"></i>
                        <img src="images/instagram.png" />
                    </a>

                    <a href="https://www.facebook.com/joaomaria.ribeirinho" target="_blank">
                        <i class="fa fa-facebook-official green" aria-hidden="true" style="font-size:30px;"></i>
                        <img src="images/facebook.png" />
                    </a>
                </div>*/?>

            </div>
        </div>
    </div>
</div>

<div class="bloco" id="formulario_fale_connosco" style="background-color: #000099">
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <div class="row">
                    <div class="col-md-12">
                        <div class="txt_titulo white center">Talk to us</div>
                    </div>
                </div>

                <br>

                <form id="form_contactos" method="post" action="">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="name" placeholder="Name" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="email" class="form-control" name="email" placeholder="Email" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="phone" placeholder="Phone" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="subject" placeholder="Subject" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <textarea rows="6" class="form-control classy-editor" name="message" placeholder="Message..."></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit">Send</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div id="contactos_response"></div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>






<?
include 'fim.php';
?>



