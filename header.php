<head>

    <meta charset="utf-8" />

    <?
    $get_url = $_SERVER["REQUEST_URI"];
    $get_url = str_replace("/", "", $get_url);
    $get_url2 = str_replace("_", " ", $get_url);
    $pagina = reset(explode("?", $get_url2));
    $pagina2 = ucwords($pagina);
    ?>

    <title>TRACKER | <?= $pagina2?></title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#363945">

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="js/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="js/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />

    <?/*<link href="js/plugins/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css" type="text/css" rel="stylesheet" />*/?>
    <link href="js/plugins/owlcarousel/owl.carousel.css" rel="stylesheet" />

    <!-- FAVICON -->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css?family=Sunflower:300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    <link href="css/cookie_bar.css" rel="stylesheet" type="text/css"/>
    <link href="css/custom.css<?= $versao?>" rel="stylesheet" type="text/css"/>
    <link href="css/login.css<?= $versao?>" rel="stylesheet" type="text/css"/>

    <link href="css/animate.css" rel="stylesheet" type="text/css"/>

    <script src="js/modernizr.custom.56918.js" type="text/javascript"></script>
    <script src="js/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>

</head>

<body>

    <?
    include 'cookies_bar.php';

    if($pagina != "admin" && $pagina != "admin_out"){
        include 'menu.php';
    }

    if($pagina != "admin" && $pagina != "admin out"){
        ?>
        <div id="loading">
            <i class="fa fa-spinner fa-spin"></i>
            <img src="images/apex-season-3-battlepass-full-icon.png" />
        </div>
        <?
    }
    ?>
