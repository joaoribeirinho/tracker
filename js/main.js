/*Código JS apenas para funções de Front-end*/
$(document).ready(function(){

    $(window).load(function () {
        //$("#loading").css("bottom", "100%");
        $("#loading").addClass("closed");
    });

/*MENU MOBILE TOGGLER*/

    $(".menu-toggler").click(function() {
        var menu = $(".menu");

        $('.menu_holder').toggleClass('open');

        menu.toggleClass("aberto");
        $('#nav-icon1').toggleClass('open');
    });

    var wer = $(".menu_holder").outerHeight();
    $(".fakebanner").css("height", wer);


    $(".search_platforms").click(function(){
        $(".search_platforms").removeClass("selected");
        $(".search_platforms[plat='" + $(this).attr("plat") + "']").addClass("selected");
    });


    $(".input_pesq").keyup(function(){
        if(event.keyCode == 13){
            $(".input_pesq i").click();
        }
    });


    $(".input_pesq i").click(function(){
        var $pesq = $("#pesq").val();
        var $plat = $(".search_platforms.selected").attr("plat");

        $.post("/processes/ajax_search.php", {pesq:$pesq, plat:$plat}, function(data) {
            $("#target").html(data);
        });
    });


    $(".banner_home li").click(function(){
        var banner_id = $(this).attr("id");

        if($(this).hasClass("full_screen")){
            $(this).removeClass("full_screen");
        }else{
            $(".banner_home li").removeClass("full_screen");
            $(this).addClass("full_screen");

            delay(function(){
                $('html, body').animate({
                    scrollTop: ( $(".banner_home li[id='" + banner_id + "']").offset().top ),
                }, 350);
            },350);
        }
    });


//------------------------------------//
//Wow Animation//
//------------------------------------//
    wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true        // trigger animations on mobile devices (true is default)
        }
    );
    wow.init();

});


var delay = (function(){
    var timer = 0;

    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();


