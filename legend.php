<?
include 'inicio.php';
?>

<div class="fakebanner22"></div>

<div class="banner">
    <ul>
        <?
        $name = $_GET["tag"];
        $get = Main::get("banners_images", "id_banner = 5 AND description LIKE '%$name%'");
        $image_path = Banners::image_path($get[0]["image"]);
        ?>
        <li style="background-image: url('<?= $image_path?>')" name="<?= $name?>">
            <div class="banner_content_box"></div>
        </li>

    </ul>
</div>

<div class="bloco block_legend">
    <div class="container">

        <div class="row">
            <?
            /*$tags = array(
                "Wraith" => "legend_1",
                "Bangalore" => "legend_2",
                "Caustic" => "legend_3",
                "Mirage" => "legend_4",
                "Bloodhound" => "legend_5",
                "Gibraltar" => "legend_6",
                "Lifeline" => "legend_7",
                "Pathfinder" => "legend_8",
                "Octane" => "legend_9",
                "Wattson" => "legend_10",
                "Crypto" => "legend_11",
            );
            $legend_id = $tags[$name];*/

            $platform = "xbl";
            $platformUserIdentifier = "SEAFOREST95";
            $legend = $_GET["tag"];

            $curl = curl_init();
            curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://public-api.tracker.gg/v2/apex/standard/profile/" . $platform . "/" . $platformUserIdentifier . "/segments/legend",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: public-api.tracker.gg",
                        "Postman-Token: 54b14f02-8395-48dd-adfd-281e6d86fa0f,c6546323-1906-4315-b479-999b8784f30c",
                        "TRN-Api-Key: 2410bdcf-4498-4ef6-b6f5-1e9059fdd0c2",
                        "User-Agent: PostmanRuntime/7.15.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache"
                    ),
                )
            );

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $response = json_decode($response);
            $response = $response->data;

            $existing_legends = 0;
            foreach($response as $item){
                if($item->metadata->name == $name){
                    $existing_legends++;
                    //debug($item);
                    $stats = $item->stats;
                    $tallImageUrl = $item->metadata->tallImageUrl;
                }
            }

            if($existing_legends == 0){
                ?>
                <div class="col-md-12 center">
                    <div class="legend_name">This player hasn't updated this legend</div>
                </div>
                <?
            }else{
                $get = Main::get("pages_images", "id_page = 1 AND description LIKE '%$name%'");
                $image_path = Pages::image_path($get[0]["image"]);
                ?>

                <div class="col-md-4 mobile_disp_none">
                    <div class="legend wow fadeInLeft" data-wow-delay="1s">
                        <img src="<?= $tallImageUrl?>" />
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="legend_name"><?= $name?></div>

                    <div class="legend_stats">
                        <?
                        foreach($stats as $stat){
                            $displayName = $stat->displayName;
                            $value = $stat->value;
                            ?>
                            <div class="each_stat_box">
                                <div class="title"><?= $displayName?></div>
                                <div class="value"><?= $value?></div>
                            </div>
                            <?
                        }
                        ?>
                    </div>

                </div>

                <div class="col-md-4 mobile_disp_block">
                    <div class="legend wow fadeInLeft" data-wow-delay="1s">
                        <img src="<?= $tallImageUrl?>" />
                    </div>
                </div>
                <?
            }
            ?>
        </div>

    </div>
</div>

<?
include 'fim.php';
?>



