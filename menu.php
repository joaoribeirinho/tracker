<div class="menu_holder">
    <div class="container">

        <div class="navbar-header">
            <a href="home">
                <img src="images/apex-season-3-battlepass-full-icon.png" id="logoimg" />
            </a>
        </div>

        <button class="menu-toggler">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>

        <div class="menu">
            <!-- <ul class="menu-nav">
                <?/*
                $menus = Menus::get("status = 1", "id ASC");

                foreach ($menus as $menu){
                    ?>
                    <li class="dropdown" id="m_<?= $menu["link"]?>">
                        <a href="<?= $menu["link"]?>"><?= $menu["menu"]?></a>
                    </li>
                    <?
                }

                //if("80.195.235.214" == "80.195.235.214"){
                if($_SERVER["REMOTE_ADDR"] == "80.195.235.214"){
                    ?>
                    <li class="dropdown" id="m_login_out">
                        <a href="logout">
                            <i class="fa fa-sign-out" aria-hidden="true"></i>
                        </a>
                    </li>
                    <?
                }*/
                ?>
            </ul>-->

            <ul class="menu-nav">

                <?/*ESTRUTURA DINAMICA DO MENU*/?>

                <?
                $menus = Menus::get("status = 1", "id ASC");
                foreach ($menus as $i => $m) {
                    if ($m["prev_menu"] == 0) {

                        ?>
                        <li class="dropdown" id="m_<?= ($m["link"] == "") ? "legend" : $m["link"]?>">
                        <?
                        $sub = Menus::get_sub_menus($m["id"]);

                        if ($sub) {

                            if($m["link"] == ""){
                                $link = "#";
                                $toggle = ' data-toggle="dropdown"';
                            }else{
                                $link = $m["link"];
                                $toggle = '';
                            }
                            // data-toggle="dropdown"

                            ?>
                            <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"<?= $toggle?> data-delay="0" data-close-others="false" href="<?= $link?>">
                                <?= $m["menu"]?> <i class="icon-angle-down"></i>
                            </a>

                            <ul class="dropdown-menu">
                                <?
                                foreach ($sub as $si => $sm) {
                                    if (!empty($m["link"])) {
                                        $slink = $sm["link"];
                                    } else {
                                        $slink = "home";
                                    }
                                    ?>
                                    <li>
                                        <a href="<?= $slink?>" class=""><?= $sm["menu"]?></a>
                                    </li>
                                    <?
                                }
                            ?>
                            </ul>
                            <?
                        }
                        else {
                            if ($m["prev_menu"] == 0) {
                                $link = $m["link"];

                                if (!empty($m["link"])) {
                                    $link = $m["link"];
                                } else {
                                    //$slink = "/$lg_activo/";
                                    $link = "javascript:;";
                                }

                                ?>
                                <a href="<?= $link?>"><?= $m["menu"]?></a>
                                <?

                            }
                        }
                        echo '</li>';
                    }
                }
                ?>

                <?/*FIM ESTRUTURA DINAMICA DO MENU*/?>

            </ul>
        </div>

    </div>
</div>
