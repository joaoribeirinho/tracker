<?
include 'inicio.php';
?>

<script>
    $("#m5").addClass("active");
    var map;
    var markers = [];
    var marker;

    var iconBase = '/images/';
    var icons = {
        info: {
            icon: iconBase + 'marker.png'
        }
    };

    function pesquisaPorRegiao(){

        //$( "html" ).addClass( "loading" );

        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('locationSelect'));

        var types = document.getElementById('type-selector');
        //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var options = {
            types: ['geocode'],
            componentRestrictions: {country: 'pt'}
        };

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            map: map,
            title: "Location Chosen!"
        });

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                window.alert("Location not found!");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(15);  // Why 17? Because it looks good.
            }

            ga('send', 'pageview','pesquisa- '+ $('#locationSelect').val());

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        });
    }

    function loadPrestadores(){
        //pesquisaTodosPrestadores();
        pesquisaPorRegiao();
    }

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: new google.maps.LatLng(39.6292721, -7.8683852),
            mapTypeId: 'roadmap',
            scrollwheel: false,
            zoomControl: true,
            mapTypeControl: false,
            fullscreenControl: false,
            streetViewControl : false,
            styles: [
                {
                    "featureType": "all",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        },
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "saturation": 36
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 40
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        },
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 17
                        },
                        {
                            "weight": 1.2
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#4d6059"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#4d6059"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#4d6059"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "lightness": 21
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#4d6059"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#4d6059"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#7f8d89"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        },
                        {
                            "lightness": 29
                        },
                        {
                            "weight": 0.2
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 18
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 16
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#7f8d89"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#000000"
                        },
                        {
                            "lightness": 19
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#2b3638"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#2b3638"
                        },
                        {
                            "lightness": 17
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#24282b"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#24282b"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                }
            ]
        });


        /* --------------- Definição da caixa de Pesquisa ------------------ */

        var input = document.getElementById('locationSelect');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            for (var i = 0, feature; feature = features[i]; i++) {
                addMarker(feature);
            }

            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {

                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }

                var marker = new google.maps.Marker({
                    position: place.geometry.location,
                    //icon: icons["info"].icon,
                    map: map
                });

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }


            });
            map.fitBounds(bounds);
        });

        /* ---------------                                 ------------------ */



        var geocoder = new google.maps.Geocoder();
        var prev_infowindow = false;

        function addMarker(feature) {

            var infowindow = new google.maps.InfoWindow({
                content: feature.content,
                maxWidth: 331
            });

            var marker = new google.maps.Marker({
                position: feature.position,
                icon: icons[feature.type].icon,
                map: map
            });

            marker.addListener('click', function() {
                if( prev_infowindow ) {
                    prev_infowindow.close();
                }

                prev_infowindow = infowindow;
                infowindow.open(map, marker);
            });

            /*if( feature.auto_open == '1' )
                infowindow.open(map, marker);*/

            markers.push(marker);
        }

        <?
        $pins = Maps_Pins::get_by_map(1, "status = '1'");
        ?>

        var features = [
            <?
            foreach( $pins as $pin) {
                $latidude = strip_tags($pin["latitude"]);
                $longitude = strip_tags($pin["longitude"]);
                $coord = $latidude . ", " . $longitude;

                if($latidude && $longitude) {
                    echo "{ 
                            position: new google.maps.LatLng(" . $coord . "),
                            type: 'info',
                            content: `<div class=\'bloco_ponto_mapa\'>
                                        <div class=\'bloco_ponto_mapa_title\'>" . $pin["name"] . "</div>
                                        <div class=\'bloco_ponto_mapa_contents\'>" . $pin["content"] . "</div>
                                    </div>` },
                        ";
                }
            }
            ?>
        ];

        for (var i = 0, feature; feature = features[i]; i++) {
            addMarker(feature);
        }

        $(document).ready( function(){
            /*$("#apoioSelect").change(function(e) {
                //e.preventDefault();

                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }

                $.post("/processes/pontos_mapa", { "apoio" : $("#apoioSelect").val() } , function( data ) {
                    //$("#resultado_ajax").html(data);
                    var pontos_array = $.parseJSON(data)
                    //console.log(pontos_array);

                    for (var i = 0, feature; feature = pontos_array[i]; i++) {

                        var feature_custom = { position : new google.maps.LatLng( feature.lat , feature.long ), type : feature.type, content : feature.content };

                        addMarker(feature_custom);
                        //console.log(feature);

                        /*
                         var infowindow = new google.maps.InfoWindow({
                         content: feature.content,
                         maxWidth: 331
                         });

                         var marker = new google.maps.Marker({
                         position: new google.maps.LatLng( feature.lat , feature.long),
                         icon: icons[feature.type].icon,
                         map: map
                         });

                         marker.addListener('click', function() {
                         infowindow.open(map, marker);
                         });

                         markers.push(marker);

                    }

                });
            });*/

            $("#apoioSelect").change(function(e) {
                // e.preventDefault();

                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }

                /*$.post("/processes/ajax_pontos_mapa", {"oficina" : $("#apoioSelect").val(), "zona" : $('option:selected', this).attr("oficina")} , function( data ) {
                     $("#resultado_ajax").html(data);


                    var pontos_array = $.parseJSON(data)

                    for (var i = 0, feature; feature = pontos_array[i]; i++) {
                        var feature_custom = {
                            position : new google.maps.LatLng( feature.lat , feature.long ),
                            type : feature.type,
                            content : feature.content,
                            auto_open : feature.auto_open,
                        };

                        addMarker(feature_custom);

                    }


                    $('html,body').animate({
                            scrollTop: $(".select_mapa_holder").offset().top},
                        'slow');

                });*/
            });

            $("#apoioSelectDistrict").change(function(e) {
                // e.preventDefault();

                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }

                //"oficina" : $("#apoioSelect").val(),

                /*$.post("/processes/ajax_pontos_mapa", {"zona" : $(this).val()} , function( data ) {
                    // $("#resultado_ajax").html(data);

                    var pontos_array = $.parseJSON(data)

                    for (var i = 0, feature; feature = pontos_array[i]; i++) {
                        var feature_custom = {
                            position : new google.maps.LatLng( feature.lat , feature.long ),
                            type : feature.type,
                            content : feature.content,
                            auto_open : feature.auto_open,
                        };

                        addMarker(feature_custom);

                    }

                    /*
                    $('html,body').animate({
                    scrollTop: $(".select_mapa_holder").offset().top},
                    'slow');


                });*/


                if($(this).val() == 0){

                    var LatLngList = [
                            new google.maps.LatLng(39.6292721, -7.868385)
                        ],
                        latlngbounds = new google.maps.LatLngBounds();

                    LatLngList.forEach(function(latLng){
                        latlngbounds.extend(latLng);
                    });

                    map.setCenter(latlngbounds.getCenter());
                    map.setZoom(7);


                }else{
                    var coordenadas_lat = $('#apoioSelectDistrict').find(":selected").attr("lat");
                    var coordenadas_lon = $('#apoioSelectDistrict').find(":selected").attr("lon");


                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(coordenadas_lat , coordenadas_lon),
                        map: map,
                    });

                    markers.push(marker);

                    var LatLngList = [
                            new google.maps.LatLng(coordenadas_lat , coordenadas_lon)
                        ],
                        latlngbounds = new google.maps.LatLngBounds();

                    LatLngList.forEach(function(latLng){
                        latlngbounds.extend(latLng);
                    });

                    map.setCenter(latlngbounds.getCenter());
                    map.setZoom(10);
                }


                /*
                $.post("/processes/pesquisa_lojas", { "distrito" : places[0].nome } , function( data ) {
                $("#listagem_lojas").html(data);
                });


                $("#locationSelect").val($(this).val());

                var e = jQuery.Event( "keydown", { keyCode: 13 } );
                $("#locationSelect").trigger(e);

                $("#locationSelect").trigger("click");
                var e = jQuery.Event("keydown");
                e.which = 13; // Enter
                $("#locationSelect").trigger(e);
                */

            });
        });

    }
</script>

<?
$google_maps_key = "AIzaSyBZwpqoS_gJbydVebH-VuyFbJhcPoSmJrI";
?>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?= $google_maps_key?>&callback=initMap&libraries=places">
</script>

<div class="fakebanner"></div>

<div class="bloco" style="padding:0">
    <div id="resultado_ajax"></div>

    <input id="locationSelect" class="controls" type="text" placeholder="Localização">

    <select id="apoioSelectDistrict">
        <option value="0">Todos Os Distritos</option>
        <option value='lisboa' lat='' lon=''>Lisboa</option>
        <option value='algarve' lat='' lon=''>Algarve</option>
        <option value='porto' lat='' lon=''>Porto</option>
    </select>

    <?/*
    <select id="apoioSelect">
        <option value="0">Concelhos</option>
        <option value="1">---------------------------</option>
    </select>
    */?>

    <div id="map"></div>

    <?/*
    <div id="listagem_lojas"></div>
    */?>

</div>

<?
include 'fim.php';
?>



