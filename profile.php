<?
include 'inicio.php';
?>

<div class="fakebanner22"></div>

<div class="banner">
    <ul>
        <?
        $banners = Banners::get_image(5, "RAND()");
        $image_path = Banners::image_path($banners[0]["image"]);
        $name = $banners[0]["description"];
        ?>
        <li style="background-image: url('<?= $image_path?>')" name="<?= $name?>">
            <div class="banner_content_box"></div>
        </li>

    </ul>
</div>


<div class="bloco block_legend">
    <div class="container">

        <div class="row">
            <?
            $platform = "xbl";
            $platformUserIdentifier = "SEAFOREST95";

            $curl = curl_init();
            curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://public-api.tracker.gg/v2/apex/standard/profile/" . $platform . "/" . $platformUserIdentifier,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: public-api.tracker.gg",
                        "Postman-Token: 54b14f02-8395-48dd-adfd-281e6d86fa0f,c6546323-1906-4315-b479-999b8784f30c",
                        "TRN-Api-Key: 2410bdcf-4498-4ef6-b6f5-1e9059fdd0c2",
                        "User-Agent: PostmanRuntime/7.15.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache"
                    ),
                )
            );

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $response = json_decode($response);
            $response = $response->data;

            $platformInfo = $response->platformInfo;
            $avatar = $platformInfo->avatarUrl;
            $platformSlug = $platformInfo->platformSlug;
            $gamertag = $platformInfo->platformUserHandle;

            $stats = $response->segments[0];
            $stats = $stats->stats;
            ?>

            <div class="col-md-4">
                <div class="profile_img">
                    <img src="<?= $avatar?>" />
                </div>
            </div>

            <div class="col-md-8">
                <div class="profile_info">
                    <div class="profile_name">
                        <img src="images/<?= $platformSlug?>.png" />
                        <span><?= $gamertag?></span>
                        <span class="smaller"> (overall)</span>
                    </div>
                </div>

                <div class="legend_stats">
                    <?
                    foreach($stats as $stat){
                        $displayName = $stat->displayName;
                        $value = $stat->value;
                        ?>
                        <div class="each_stat_box">
                            <div class="title"><?= $displayName?></div>
                            <div class="value"><?= $value?></div>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="bloco bloco2">
    <div class="container">

        <div style="">

            <?
            $legends = Pages::get_image(1);
            foreach($legends as $legend){
                $image_path = Pages::image_path($legend["image"]);
                $name = $legend["description"];
                ?>
                <div class="col-md-3 col-xs-6 legend_holder">
                    <div class="each_legend" onclick="location.href='legend?tag=<?= $name?>'">
                        <img src="<?= $image_path?>" />

                        <div class="nametag"><?= $name?></div>
                    </div>
                </div>
                <?
            }
            ?>

        </div>

    </div>
</div>

<?

include 'fim.php';
?>



