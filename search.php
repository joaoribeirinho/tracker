<?
include 'inicio.php';
?>

<div class="fakebanner22"></div>

<div class="banner">
    <ul>
        <?
        $banners = Banners::get_image(5, "RAND()");
        $image_path = Banners::image_path($banners[0]["image"]);
        $name = $banners[0]["description"];
        ?>
        <li style="background-image: url('<?= $image_path?>')" name="<?= $name?>">
            <div class="banner_content_box"></div>
        </li>

    </ul>

    <div class="bloco block_search">
        <div class="container">
            <div class="pad_only_bot">
                <div class="col-md-12 center">
                    <div class="disp_inline_flex">
                        <div class="search_platforms" plat="origin">
                            <img src="images/origin.png" />
                        </div>
                        <div class="search_platforms selected" plat="xbl">
                            <img src="images/xbl.png" />
                        </div>
                        <div class="search_platforms" plat="psn">
                            <img src="images/psn.png" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-md-offset-2">
                <div class="input_pesq">
                    <input id="pesq" class="form-control" placeholder="Gamertag here..." type="text">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="target"></div>

<?
include 'fim.php';
?>



